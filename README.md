# Contents

This is just a Rust implementation of the 'Polynomial' based on [A Programmer's Introduction to Mathematics](https://pimbook.org/)

Example usage:
```rust
use polynomial::{Polynomial, interpolate};

fn main() {
    let p = Polynomial::new(& [1.0,2.0,3.0,0.0,4.0]);
    let x = 1.0;
    print!("{} = ", p);
    println!("{}; for x = {}", p.evaluate(&x), x);

    let xs = vec![0.0, 2.0];
    let ys = vec![5.0, 9.0];
    println!("\nGiven points: ");
    for i in 0..xs.len() {
        println!("({},{})", xs.get(i).unwrap(), ys.get(i).unwrap());
    }
    let ip = interpolate(&xs, &ys);
    println!("Interpolate polynomial: {}", ip);
}
```
Output:
```
f(x) = 1 + 2x + 3x^2 + 4x^4  = 10; for x = 1

Given points: 
(0,5)
(2,9)
Interpolate polynomial: f(x) = 5 + 2x 
```
