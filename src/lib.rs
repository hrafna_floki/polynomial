#![crate_name = "polynomial"]

use std::fmt::{self, Formatter, Display};
use std::ops::{Add, Mul};

/// Represents a Polynomial f(x) = c0*x^0 + c1*x^1 + ... + cn*x^n ;
/// based on the book "A Programmer's Introduction to Mathematics"
/// https://pimbook.org/
#[derive(Debug, Clone)]
pub struct Polynomial {
    coefficients: Vec<f64>,
}

impl Polynomial {
    /// Returns a new Polynomial
    /// # Arguments
    /// * `data` - Array of coefficients starting with coefficient for x^0
    /// # Examples
    /// ```
    /// // Polynomial for f(x) = 1 + 2x + 4x^3
    /// use polynomial::Polynomial;
    /// let p = Polynomial::new(&[1.0, 2.0, 0.0, 4.0]);
    /// ```
    pub fn new(data: &[f64]) -> Polynomial {
        let mut p = Polynomial {
            coefficients: Vec::new(),
        };
        let mut is_zero = true;
        for x in data {
            if *x != 0.0 {
                is_zero = false;
                break;
            }
        }
        if !is_zero {
            for x in data {
                p.coefficients.push(x.clone());
            }
        }
        p
    }

    /// Adds a Polynomial to this one;
    /// # Arguments
    ///  * other - The Polynomial to add
    /// # Examples
    /// ```
    /// use polynomial::Polynomial;
    /// let p1 = Polynomial::new(&[1.0,2.0,3.0]);
    /// let p2 = Polynomial::new(&[4.0,5.0,6.0]);
    /// let p = p1.add(p2);
    /// assert_eq!(p.get_coefficients(), &[5.0, 7.0, 9.0]);
    /// ```
    pub fn add(self, other: Polynomial) -> Polynomial {
        let max_index = self.coefficients.len().max(
            other.coefficients.len());
        let mut new_coeff = vec![0.0; max_index];
        for i in 0..max_index {
            let a = self.coefficients.get(i).unwrap_or(&0.0);
            let b = other.coefficients.get(i).unwrap_or(&0.0);
            let c = new_coeff.get_mut(i).unwrap();
            *c = a + b;
        }
        Polynomial::new(&new_coeff.as_slice())
    }

    /// Mutliplies a Polynomial with this one.
    /// # Arguments
    ///  * other - The Polynomial to multiply with
    /// # Examples
    /// ```
    /// use polynomial::Polynomial;
    /// let p1 = Polynomial::new(&[1.0,2.0,3.0]);
    /// let p2 = Polynomial::new(&[4.0,5.0,6.0]);
    /// let p = p1.multiply(p2);
    /// assert_eq!(p.get_coefficients(), &[4.0, 13.0, 28.0, 27.0, 18.0]);
    /// ```
    pub fn multiply(self, other: Polynomial) -> Polynomial {
        let new_size = self.coefficients.len()
            + other.coefficients.len() - 1;
        let mut new_coeff = vec![0.0; new_size];
        for i in 0..self.coefficients.len() {
            let a = self.coefficients.get(i).unwrap();
            for j in 0..other.coefficients.len() {
                let b = other.coefficients.get(j).unwrap();
                let c = new_coeff.get_mut(i+j).unwrap();
                *c = *c + a * b;
            }
        }
        Polynomial::new(&new_coeff.as_slice())
    }

    /// Evaluates the Polynomial for a given x
    /// # Arguments
    /// * x - The value to evaluate the Polynomial at.
    /// # Examples
    /// ```
    /// use polynomial::Polynomial;
    /// let mut p = Polynomial::new(&[1.0,2.0,3.0,0.0,4.0]);
    /// assert_eq!(p.evaluate(&1.0), 10.0);
    /// ```
    pub fn evaluate(&self, x: &f64) -> f64 {
        let mut res = 0.0;
        for coeff in self.coefficients.iter().rev() {
            res = res * x + coeff;
        }
        res
    }

    /// Get the degree of the Polynomial
    /// # Examples
    /// ```
    /// use polynomial::Polynomial;
    /// let mut p = Polynomial::new(&[1.0,2.0,3.0,0.0,4.0]);
    /// assert_eq!(p.degree().unwrap(), 4);
    /// ```
    pub fn degree(&self) -> Option<usize> {
        if !self.coefficients.is_empty() {
            Option::Some(self.coefficients.len() - 1)
        }
        else {
            Option::None
        }
    }

    /// Get the coefficients of the Polynomial, starting with x^0
    /// # Examples
    /// ```
    /// use polynomial::Polynomial;
    /// let p = Polynomial::new(&[1.0,2.0,3.0,0.0,4.0]);
    /// assert_eq!(p.get_coefficients(), &[1.0,2.0,3.0,0.0,4.0]);
    /// ```
    pub fn get_coefficients(&self) -> &[f64] {
        self.coefficients.as_slice()
    }
}

impl Display for Polynomial {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "f(x) = ").unwrap();
        for i in 0..self.coefficients.len() {
            let value = self.coefficients.get(i).unwrap();
            if *value == 0.0 {
                continue;
            }
            write!(f, "{}", value).unwrap();
            let x = match i {
                0 =>  String::from(" "),
                1 =>  String::from("x "),
                _ => format!("x^{} ", i),
            };
            write!(f, "{}", x.as_str()).unwrap();
            if i < self.coefficients.len() - 1 {
                write!(f, "+ ").unwrap();
            }
        }
        fmt::Result::Ok(())
    }
}

impl Add for Polynomial {
    type Output = Polynomial;

    fn add(self, rhs: Self) -> Polynomial {
        self.add(rhs)
    }
}

impl Mul for Polynomial {
    type Output = Polynomial;

    fn mul(self, rhs: Self) -> Polynomial {
        self.multiply(rhs)
    }
}

impl PartialEq for Polynomial {
    fn eq(&self, other: &Self) -> bool {
        self.coefficients == other.coefficients
    }
}
impl Eq for Polynomial {}

/// Interpolate a Polynomial whichs connects all the
/// given points.
/// # Arguments
/// * xs - The x coordinates of the points
/// * ys - The y coordinates of the points
/// # Examples
/// ```
/// use polynomial::{Polynomial, interpolate};
/// // Two points (0, 5) and (5, 9) to interpolate
/// // a degree one polynomial f(x) = 5 + 2x
/// let xs = vec![0.0, 2.0];
/// let ys = vec![5.0, 9.0];
/// let p = Polynomial::new(&[5.0,2.0]);
/// let ip = interpolate(&xs, &ys);
/// assert_eq!(p, ip);
/// ```
pub fn interpolate(xs: &Vec<f64>, ys: &Vec<f64>) -> Polynomial {
    assert!(!xs.is_empty(), "No points provided.");
    assert_eq!(xs.len(), ys.len(), "x and y coordinate lists \
     must be the same length.");
    let mut res = Polynomial::new(&[0.0]);
    for i in 0..xs.len() {
        let term = single_term(xs, ys, i);
        res = res + term;
    }
    res
}

/// Interpolates a particular term for a polynomial for the
/// given index.
/// # Arguments
/// * xs - The x coordinates of the points
/// * ys - The y coordinates of the points
/// * index = The index of the term to interpolate
fn single_term(xs: &Vec<f64>, ys: &Vec<f64>, index: usize) -> Polynomial {
    let mut the_term = Polynomial::new(&[1.0]);

    let x_index = xs.get(index).unwrap();
    let y_index = ys.get(index).unwrap();

    for i in 0..xs.len() {
        if i == index {
            continue;
        }
        let xi = xs.get(i).unwrap();
        let coeff = [-xi / (x_index - xi), 1.0 / (x_index - xi)];
        let p2 = Polynomial::new(&coeff);
        the_term = the_term * p2;
    }
    the_term * Polynomial::new(&[*y_index])
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_zero() {
        let p = Polynomial::new(&[]);
        assert_eq!(p.degree(), Option::None);
        let p = Polynomial::new(&[0.0]);
        assert_eq!(p.degree(), Option::None);
        let p = Polynomial::new(&[0.0, 0.0]);
        assert_eq!(p.degree(), Option::None);
    }

    #[test]
    fn test_evaluate() {
        let p = Polynomial::new(&[1.0,2.0,3.0,0.0,4.0]);
        assert_eq!(p.degree().unwrap(), 4);
        assert_eq!(p.evaluate(&1.0), 10.0);
        assert_eq!(p.evaluate(&2.0), 81.0);
    }

    #[test]
    fn test_add() {
        let p1 = Polynomial::new(&[1.0,2.0,3.0]);
        let p2 = Polynomial::new(&[4.0,5.0,6.0]);
        let p = p1 + p2;
        assert_eq!(p.degree().unwrap(), 2);
        assert_eq!(p.get_coefficients(), &[5.0, 7.0, 9.0]);
    }

    #[test]
    fn test_multiply() {
        let p1 = Polynomial::new(&[1.0,2.0,3.0]);
        let p2 = Polynomial::new(&[4.0,5.0,6.0]);
        let p = p1 * p2;
        assert_eq!(p.degree().unwrap(), 4);
        assert_eq!(p.get_coefficients(), &[4.0, 13.0, 28.0, 27.0, 18.0]);
    }

    #[test]
    fn test_interpolate() {
        let xs = vec![1.0, 2.0, -3.0, 4.0];
        let ys = vec![1.0, 0.0, 2.0, 4.0];
        let ip = interpolate(&xs, &ys);
        for i in 0..xs.len() {
            let yi = ip.evaluate(xs.get(i).unwrap()).round();
            assert_eq!(ys.get(i).unwrap().to_owned(), yi);
        }
    }

    #[test]
    fn test_interpolate_2() {
        let xs = vec![0.0, 2.0];
        let ys = vec![5.0, 9.0];
        let ip = interpolate(&xs, &ys);
        let p = Polynomial::new(&[5.0,2.0]);
        assert_eq!(ip, p);
    }
}
