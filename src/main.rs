use polynomial::{Polynomial, interpolate};

fn main() {
    let p = Polynomial::new(& [1.0,2.0,3.0,0.0,4.0]);
    let x = 1.0;
    print!("{} = ", p);
    println!("{}; for x = {}", p.evaluate(&x), x);

    let xs = vec![0.0, 2.0];
    let ys = vec![5.0, 9.0];
    println!("\nGiven points: ");
    for i in 0..xs.len() {
        println!("({},{})", xs.get(i).unwrap(), ys.get(i).unwrap());
    }
    let ip = interpolate(&xs, &ys);
    println!("Interpolate polynomial: {}", ip);
}
